# PruebasJavaTime

Pruebas de manejo de fechas **java.time** (API más reciente de fechas introducida en Java 8) y **java.util**. Uso de formatos por defecto de Java y cambios de locales.

Paso de **java.sql.Timestamp** (suponiendo UTF-8) a **java.time.ZonedDateTime** (con el local del sistema).

Cambios entre repositorios de locales [CLDR (repositorio de unicode)](http://cldr.unicode.org/) y el propio del JRE en Java 8 y 11.


### Notas:

El local euskera (eu-ES) está bien soportado en Java 11 con CLDR. Usando CLDR en Java 8 salen algunos errores en la escritura de los meses:

```java
Locale l = new Locale("eu","ES");

GregorianCalendar cal = new GregorianCalendar();
ZonedDateTime z = cal.toZonedDateTime().minusMonths(2);

//java.time y datetimeformatter
DateTimeFormatter df1 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
	.withLocale(l);
System.out.println(l.toString() + " " + df1.format(z));
		
DateTimeFormatter df11 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
	.withLocale(l);
System.out.println(l.toString() + " " + df11.format(z));

DateTimeFormatter df12 = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
	.withLocale(l);
System.out.println(l.toString() + " " + df12.format(z));
			
DateTimeFormatter df2 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
	.withLocale(l);
System.out.println(l.toString() + " " + df2.format(z));

//java.util y dateformat

Date today = new Date();
			
DateFormatter dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, l);
System.out.println(dateFormatter.format(today));

dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT, l);
System.out.println(dateFormatter.format(today));
			
dateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM, l);
System.out.println(dateFormatter.format(today));
			
dateFormatter = DateFormat.getDateInstance(DateFormat.LONG, l);
System.out.println(dateFormatter.format(today));
			
dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, l);
System.out.println(dateFormatter.format(today));
```

Resultados Java 11 (CLDR):

```
eu_ES 2019(e)ko ekainaren 13(a), osteguna 09:51:48 (Europako erdialdeko udako ordua)
eu_ES 2019(e)ko ekainaren 13(a) 09:51:48 (CEST)
eu_ES 2019(e)ko ekainaren 13(a), osteguna
eu_ES 2019 eka. 13 09:51:48

2019 abu. 13
19/8/13
2019 abu. 13
2019(e)ko abuztuaren 13(a)
2019(e)ko abuztuaren 13(a), asteartea
```

Resultados JAVA 8 (CLDR):

```
eu_ES osteguna, 2019eko ekainaren 13a 10:27:50 Central European Summer Time
eu_ES 2019eko ekaren 13a 10:27:50 CEST
eu_ES osteguna, 2019eko ekainaren 13a
eu_ES 2019 eka 13 10:27:50

2019 abu 13
2019-08-13
2019 abu 13
2019eko aburen 13a
asteartea, 2019eko abuztuaren 13a
```