package org.pruebas.locales;

import java.util.Arrays;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.chrono.IsoChronology;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.FormatStyle;
import java.time.format.TextStyle;;

public class LocalizedDatesMain {
	
	public static void main(String[] args) {
		
		System.out.println(Arrays.toString(Locale.getISOCountries()));
		
		System.out.println(Arrays.toString(Locale.getAvailableLocales()));
		
		System.out.println(Arrays.toString(Locale.getISOLanguages()));
		
		Locale es = new Locale("es","ES");
		Locale eus = new Locale("eu","ES");
		Locale en = new Locale("en", "GB");
		
		Locale[] loc = {es, eus, en};
		
		LocalDateTime date = LocalDateTime.now();
		System.out.println(DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.LONG, FormatStyle.LONG, IsoChronology.INSTANCE, es));
		System.out.println(DateTimeFormatterBuilder.getLocalizedDateTimePattern(FormatStyle.FULL, FormatStyle.LONG, IsoChronology.INSTANCE, eus));
		
		System.out.println(date);
		
		DayOfWeek m = DayOfWeek.MONDAY;
		
		System.out.println(m.getDisplayName(TextStyle.FULL, eus));
		System.out.println(m.getDisplayName(TextStyle.FULL, es));
		
		
		String fecha = ZonedDateTime       // Use modern class for a moment seen from a particular time zone.
		.now()              // Capture the current moment, using the JVM�s current default time zone. Better to specify a zone explicitly.
		.format(            // Generate a `String` representing the value of our `ZonedDateTime` object.
		    DateTimeFormatter.ofLocalizedDateTime( FormatStyle.FULL )
		    .withLocale(eus) 
		);                   // Returns a `String` object.
		System.out.println(fecha);
		
		Date fech = new Date();
		System.out.println(Arrays.toString(DateFormat.getAvailableLocales()));
		
		
		////////////
		System.out.println("++++++++++++++++++++++java.time y datetimeformatter++++++++++++++++++++++++++++++++");
		GregorianCalendar cal = new GregorianCalendar();
		
		for(Locale l:loc){
			DateTimeFormatter df = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.FULL)
					.withLocale(l);
			
			ZonedDateTime z = cal.toZonedDateTime().minusMonths(2);
			System.out.println(l.toString() + " " + df.format(z));
			
			DateTimeFormatter df1 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG)
					.withLocale(l);
			System.out.println(l.toString() + " " + df1.format(z));
			
			DateTimeFormatter df12 = DateTimeFormatter.ofLocalizedDate(FormatStyle.FULL)
					.withLocale(l);
			System.out.println(l.toString() + " " + df12.format(z));
			
			DateTimeFormatter df11 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)
					.withLocale(l);
			System.out.println(l.toString() + " " + df11.format(z));
			
			DateTimeFormatter df2 = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)
					.withLocale(l);
			System.out.println(l.toString() + " " + df2.format(z));
			
			DateTimeFormatter df3 = DateTimeFormatter.ISO_LOCAL_DATE
					.withLocale(l);
			System.out.println(l.toString() + " " + df3.format(z));
			
			DateTimeFormatter df4 = DateTimeFormatter.ISO_DATE
					.withLocale(l);
			System.out.println(l.toString() + " " + df4.format(z));
			
			
			DateTimeFormatter df5 = DateTimeFormatter.ofPattern("dd/MMMM/yy")
					.withLocale(l);
			System.out.println(l.toString() + " " + df5.format(z));
			
			DateTimeFormatter df6 = DateTimeFormatter.ofPattern("dd/MMMM/yyyy")
					.withLocale(l);
			System.out.println(l.toString() + " " + df6.format(z));
			
			
			
			System.out.println();
		}
		
		
System.out.println("+++++++++++++++++++++++java.util y dateformat+++++++++++++++++++++++++++++");
		
		Date today;
		DateFormat dateFormatter;
		
		for(Locale l:loc){
			today = new Date();
			
			System.out.println("+++++++++Formatos java++++++");
			dateFormatter = DateFormat.getDateInstance(DateFormat.DEFAULT, l);
			System.out.println(dateFormatter.format(today));
			
			dateFormatter = DateFormat.getDateInstance(DateFormat.SHORT, l);
			System.out.println(dateFormatter.format(today));
			
			dateFormatter = DateFormat.getDateInstance(DateFormat.MEDIUM, l);
			System.out.println(dateFormatter.format(today));
			
			dateFormatter = DateFormat.getDateInstance(DateFormat.LONG, l);
			System.out.println(dateFormatter.format(today));
			
			dateFormatter = DateFormat.getDateInstance(DateFormat.FULL, l);
			System.out.println(dateFormatter.format(today));
			
			
			System.out.println("++++++++++++Simpledateformater++++++++++++");
			
			SimpleDateFormat sdf;
			String[] formats = {"yyyy-M-d", "yyyy-MM-dd", "yy/MMM/dd", "yy/MMMM/dd", "yyyy / MMM / dd"};
			for(String f: formats) {
				sdf = new SimpleDateFormat(f, l);
				System.out.println(sdf.format(today));
			}
			
			System.out.println();
		}
	}

}
