package org.pruebas.locales;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.TimeZone;

public class UtilTimeParsingMain {

	public static void main(String[] args) {
		System.out.println("++++zoneids disponibles+++++");
		System.out.println(Arrays.toString(ZoneId.getAvailableZoneIds().toArray()));
		
		System.out.println("++++++++++++++prueba+++++++++++++++++++");
		Date d = new Date();
		System.out.println(d.toString());
		
		Instant i = d.toInstant();
		System.out.println(i.toString());
		
		LocalDateTime ldt = LocalDateTime.ofInstant(i, ZoneId.of("GMT"));
		System.out.println(ldt.toString());
		
		
		System.out.println("++++++++++++++prueba+++++++++++++++++++");
		Calendar c = new GregorianCalendar(2007, 1, 20, 11, 10);
		System.out.println(c.toString());
		System.out.println(c.getTime().toString());
		
		c.setTimeZone(TimeZone.getTimeZone(ZoneId.of("GMT")));//no lo coge
		System.out.println(c.toString());
		System.out.println(c.getTime().toString());
		
		Instant i2 = c.toInstant();
		System.out.println(i2.toString());
		
		LocalDateTime ldt2 = LocalDateTime.ofInstant(i2, ZoneId.of("Europe/Madrid"));
		System.out.println(ldt2.toString());

		
		System.out.println("++++++++++++++prueba+++++++++++++++++++");
		Calendar c2 = new GregorianCalendar(TimeZone.getTimeZone(ZoneId.of("GMT")));
		System.out.println(c2.toString());
		System.out.println(c2.getTime().toString());

		Instant i3 = c2.toInstant();
		System.out.println(i3.toString());
		
		LocalDateTime ldt3 = LocalDateTime.ofInstant(i3, ZoneId.of("Europe/Madrid"));
		System.out.println(ldt3.toString());
		
		
		DateTimeFormatter dateFormatter = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.LONG);
		
		System.out.println("++++++++++++++prueba+++++++++++++++++++");
		java.sql.Timestamp sqld = new Timestamp(System.currentTimeMillis());
		System.out.println(sqld.toString());

		Instant i4 = sqld.toInstant();
		System.out.println(i4.toString());
		
		LocalDateTime ldt4 = LocalDateTime.ofInstant(i4, ZoneId.of("GMT"));
		System.out.println(ldt4.toString());
		
		System.out.println("Conversion directa desde el timestamp, conserva la hora");
		LocalDateTime ldt5 = sqld.toLocalDateTime();
		System.out.println(ldt5.toString() +" ");
		
		System.out.println("Si se asume que la hora est� en utc, se convierte a su zona:");
		System.out.println(ZonedDateTime.of(ldt5, ZoneId.of("GMT")).toString());
		System.out.println(dateFormatter.format(ZonedDateTime.of(ldt5, ZoneId.of("GMT"))));
		System.out.println(ZonedDateTime.of(ldt5, ZoneOffset.systemDefault()).toString());
		System.out.println(dateFormatter.format(ZonedDateTime.of(ldt5, ZoneOffset.systemDefault())));
		
		System.out.println(ZonedDateTime.of(ldt5, ZoneId.of("GMT")).withZoneSameInstant(ZoneId.systemDefault()).toString());
		System.out.println(dateFormatter.format(ZonedDateTime.of(ldt5, ZoneId.of("GMT")).withZoneSameInstant(ZoneId.systemDefault())));
		
		
		System.out.println("+++++++++++++++++++++++Paso de sql timestamp en UTC a local+++++++++++++++++");
		java.sql.Timestamp sqld1 = new Timestamp(System.currentTimeMillis());
		System.out.println("Hora sql: " + sqld1.toString());
		ZonedDateTime zdtutc = ZonedDateTime.of(sqld1.toLocalDateTime(), ZoneId.of("GMT"));
		System.out.println("Hora ZonedDateTime en UTC " + dateFormatter.format(zdtutc));
		ZonedDateTime zdtloc = zdtutc.withZoneSameInstant(ZoneId.systemDefault());
		System.out.println("Hora ZonedDateTime en Local " + dateFormatter.format(zdtloc));
		
		System.out.println("++++++++++++++++Paso inverso, de fecha en local a utc para BD+++++++++++++++");
		ZonedDateTime zdt = ZonedDateTime.now();
		System.out.println("Hora ZonedDateTime en local " + dateFormatter.format(zdt));
		ZonedDateTime zdtut = zdt.withZoneSameInstant(ZoneId.of("GMT"));
		System.out.println("Hora ZonedDateTime en UTC " + dateFormatter.format(zdtut));
		java.sql.Timestamp sqd = Timestamp.valueOf(zdtut.toLocalDateTime());
		System.out.println("Hora sql: " + sqd.toString());
	}

}
